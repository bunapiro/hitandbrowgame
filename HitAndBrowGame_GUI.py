# coding:utf-8
import random
import re
import tkinter as tk
import tkinter.messagebox as tkmsg

# Fixed Value
MAX_DATA_NUM = 4


"""""""""""""""""""""
 4桁のランダムの数値を生成する
"""""""""""""""""""""
def CreateRandomNumber():

    # 4桁のランダムの数値を生成する
    CorrectData = []
    for i in range( MAX_DATA_NUM ):
        CorrectData.append( random.randint(0, 9) )


    # Debug用にランダムで生成した値を表示する
    print( str(CorrectData[0]) + str(CorrectData[1]) + str(CorrectData[2]) + str(CorrectData[3]) )

    return CorrectData


"""""""""""""""""""""
 ユーザから入力された数値を返す
"""""""""""""""""""""
def GetInputData():

    fValidInput = False

    # 入力値を取得
    InputData = EditBox1.get()
        
    # 入力された数値が4桁の数値であるかチェック
    if not re.match( r"^\d\d\d\d$", InputData ):
        tkmsg.showerror("エラー", "入力値が不正です")
    else:
        fValidInput = True
        
        
    return fValidInput, InputData


"""""""""""""""""""""
 ヒット数を算出する
"""""""""""""""""""""
def CalcHitNum( CorrectData, InputData ):

    # ヒット数を算出する
    HitNum = 0    
    for i in range( MAX_DATA_NUM ):
        if CorrectData[i] == int(InputData[i]):
            HitNum = HitNum + 1


    return HitNum


"""""""""""""""""""""
 ブロー数を算出する
"""""""""""""""""""""
def CalcBlowNum( CorrectData, InputData ):

    BlowNum = 0
    for j in range( MAX_DATA_NUM ):
        for i in range( MAX_DATA_NUM ):
            if (    ( CorrectData[i] == int(InputData[j]) )    \
                and ( CorrectData[i] != int(InputData[i]) )    \
                and ( CorrectData[j] != int(InputData[j]) )):

                BlowNum = BlowNum + 1
                break

    
    return BlowNum


"""""""""""""""""""""
 ゲーム終了判定
"""""""""""""""""""""
def IsGameFinish( HitNum ):

    IsFinish = False

    # 全てヒットしたらゲーム終了
    if HitNum == MAX_DATA_NUM:
        IsFinish = True
    
    return IsFinish


"""""""""""""""""""""
 結果を表示する
"""""""""""""""""""""
def ShowResult( HitNum, BlowNum ):

    if IsGameFinish( HitNum ):
        tkmsg.showinfo("結果", "おめでとうございます！ 当りです！" )
        GameWindow.destroy()
    else:
        tkmsg.showinfo( "結果", "ヒット数：" + str(HitNum) + "\n" + "ブロー数：" + str(BlowNum)  )
        

"""""""""""""""""""""
 回答履歴に記録
"""""""""""""""""""""
def MemoryAnswerInfo( InputData, HitNum, BlowNum ):

    HistoryBox.insert( tk.END, InputData + " /  Hit:" + str(HitNum) + "  Blow:" + str(BlowNum) + "\n" )


"""""""""""""""""""""
 ボタンがクリックされた時の処理
"""""""""""""""""""""
def ButtonClick():

    # 入力値を取得する
    fValidInput, InputData = GetInputData()

    # 正誤判定
    if fValidInput:

        # ヒット数を算出する
        HitNum = CalcHitNum( CorrectData, InputData )

        # ブロー数を算出する
        BlowNum = CalcBlowNum( CorrectData, InputData )

        # 結果を表示
        ShowResult( HitNum, BlowNum )

        # 回答履歴に記録
        MemoryAnswerInfo( InputData, HitNum, BlowNum )
    
    # 入力欄をクリアする
    EditBox1.delete( 0, tk.END )


"""""""""""""""""""""
 main関数
"""""""""""""""""""""

# 4桁のランダムの数値を生成する
CorrectData = CreateRandomNumber()

# Windowの生成
GameWindow = tk.Tk()
GameWindow.geometry("600x400")
GameWindow.title("数当てゲーム")

# Labelの生成
Label1 = tk.Label( GameWindow, text="数値を入力してください", font=("Helvetica", 14) )
Label1.place( x=20, y=20 )

# TextBoxの生成
EditBox1 = tk.Entry( width=4, font=("Helvetica", 28) )
EditBox1.place( x=120, y=60 )

# 入力履歴TextBoxの生成
HistoryBox = tk.Text( GameWindow, font=("Helvetica", 14) )
HistoryBox.place( x=400, y=0, width=200, height=400 )

# Buttonの生成
CheckButton = tk.Button( GameWindow, text="チェック", font=("Helvetica", 14), command=ButtonClick )
CheckButton.place( x=220, y=60 )

# Windowの表示
GameWindow.mainloop()
