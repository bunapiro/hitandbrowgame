# coding:utf-8
import random
import re

# Fixed Value
MAX_DATA_NUM = 4



"""""""""""""""""""""
 4桁のランダムの数値を生成する
"""""""""""""""""""""
def CreateRandomNumber():

    # 4桁のランダムの数値を生成する
    CorrectData = []
    for i in range( MAX_DATA_NUM ):
        CorrectData.append( random.randint(0, 9) )


    # Debug用にランダムで生成した値を表示する
    print( str(CorrectData[0]) + str(CorrectData[1]) + str(CorrectData[2]) + str(CorrectData[3]) )

    return CorrectData


"""""""""""""""""""""
 ユーザから入力された数値を返す
"""""""""""""""""""""
def GetInputData():

    fValidInput = False

    # 有効な数値が入力されるまで繰り返す
    while fValidInput == False:

        # 回答入力を促すメッセージを表示
        InputData = input("\n4桁の数字を入力してください\n>")

        
        # 入力された数値が4桁の数値であるかチェック
        """
        if  len(InputData) != MAX_DATA_NUM:
            print("4桁の数字を入力してください\n")
        else:

            # 入力された4桁の値が0~9の数値であるかチェック
            for i in range( MAX_DATA_NUM ):
                if ( InputData[i] < "0" ) or ( InputData[i] > "9" ):
                    print("数字ではない入力値があります\n")
                    break
            # ループが最後まで回りきったら正当な入力値
            else:
                fValidInput = True
        """
        if not re.match( r"^\d\d\d\d$", InputData ):
            print("入力値が不正です")
        else:
            fValidInput = True
        
        
        # 入力された値が4桁の数値であることが確認できた
        if fValidInput:
            break
    
    return InputData


"""""""""""""""""""""
 ヒット数を算出する
"""""""""""""""""""""
def CalcHitNum( CorrectData, InputData ):

    # ヒット数を算出する
    HitNum = 0    
    for i in range( MAX_DATA_NUM ):
        if CorrectData[i] == int(InputData[i]):
            HitNum = HitNum + 1


    return HitNum


"""""""""""""""""""""
 ブロー数を算出する
"""""""""""""""""""""
def CalcBlowNum( CorrectData, InputData ):

    BlowNum = 0
    for j in range( MAX_DATA_NUM ):
        for i in range( MAX_DATA_NUM ):
            if (    ( CorrectData[i] == int(InputData[j]) )    \
                and ( CorrectData[i] != int(InputData[i]) )    \
                and ( CorrectData[j] != int(InputData[j]) )):

                BlowNum = BlowNum + 1
                break

    
    return BlowNum


"""""""""""""""""""""
 結果を表示する
"""""""""""""""""""""
def PrintResult( HitNum, BlowNum ):

    # 結果を表示
    print( "ヒット数：" + str(HitNum) )
    print( "ブロー数：" + str(BlowNum) )


"""""""""""""""""""""
 ゲーム終了判定
"""""""""""""""""""""
def IsGameFinish( HitNum ):

    IsFinish = False

    # 全てヒットしたらゲーム終了
    if HitNum == MAX_DATA_NUM:
        print("当り！")
        IsFinish = True
    
    return IsFinish


"""""""""""""""""""""
 main関数
"""""""""""""""""""""

# 4桁のランダムの数値を生成する
CorrectData = CreateRandomNumber()

# Hit & Blow Game Start !!
while True:

    # 入力値を取得する
    InputData = GetInputData()

    # ヒット数を算出する
    HitNum = CalcHitNum( CorrectData, InputData )
    
    # ブロー数を算出する
    BlowNum = CalcBlowNum( CorrectData, InputData )

    # 結果を表示
    PrintResult( HitNum, BlowNum )

    # ゲーム終了判定
    if IsGameFinish( HitNum ):
        break


